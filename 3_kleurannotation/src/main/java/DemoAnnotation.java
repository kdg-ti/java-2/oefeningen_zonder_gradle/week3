import annotations.MyAnnotations;
import annotations.ParserClass;

public class DemoAnnotation {
    public static void main(String[] args) {
        ParserClass.execute(MyAnnotations.class);
    }
}

// Verwachte afdruk:
/*
Het huis is ROOD
De wagen is BLAUW
*/